
# How python optimizations interact with Numba 

Numpy arrays are mostly homogenous, which helps numba to infer the datatypes therefor it passes to the static typing. 
basically they say that numba understands calls to NumpY *ufuncs*(universal functions (see this link)[https://docs.scipy.org/doc/numpy/reference/ufuncs.html]) and it is able to translate them into a native code. 
NumPy arrays are directly supported in Numba. Access to Numpy arrays is very efficient, as indexing is lowered to direct memory accesses when possible

