The energy consumption of software systems is becoming a critical concern with the continuous growth of digital services.
In particular, recent studies forecasted that ICT is going to be responsible for 20\,\% of all the world’s electricity by 2025.\footnote{\url{https://www.theguardian.com/environment/2017/dec/11/tsunami-of-data-could-consume-fifth-global-electricity-by-2025}}
In the context of ICT, software systems are playing a central role by driving the power consumption of hardware resources (CPU, GPU, disk, network, etc.).
Nonetheless, we believe that software performance bottlenecks inevitably lead to power consumption wastes, thus contributing to the impact of computing devices on the global emission.
Therefore, beyond key advances achieved on increasing the power efficiency of hardware components (\emph{e.g.}, P-States and C-States features promoted by modern CPU), one can expect that improving the power efficiency of software systems can catalyze hardware optimizations and thus contribute to long-term sustainability, thus reducing the impact of ICT.

When it comes to developing software systems, one can notice that---except Perl---dynamic programming languages have taken over compiled languages along the last decade in terms of developers popularity (cf. Figure~\ref{fig:pypl}).
However, it remains unclear if this class of dynamic programming languages can reasonably compete with compiled ones when it comes to power efficiency.

\begin{figure}[htbp]
    \includegraphics[width=\linewidth]{figures/programminglanguangespopularity.png}
    \caption{PYPL Popularity of Programming Languages~\cite{noauthor_pypl_nodate}}
    \label{fig:pypl}
\end{figure}

In particular, Noureddine~\emph{et~al.}~\cite{noureddine_preliminary_2012} in 2012, and then Pereira~\emph{et~al.}~\cite{pereira_energy_2017} in 2017, conducted empirical power measurements on this topic, and both concluded that compiled programming languages overcome dynamic ones when it comes to power consumption.
According to their experiments, an interpreted programming language like Python can impose up to a $7,588\,\%$ energy overhead compared to C~\cite{pereira_energy_2017} (cf. Figure~\ref{fig:hannoi}).
% However, the operational conditions under which these measurements may threaten the validity of their results.

\begin{figure}[htbp]
    \includegraphics[width=\linewidth]{figures/hannoiimplementation.png}
    \caption{Energy consumption of a recursive implementation of Tower of Hanoi program in different languages~\cite{noureddine_preliminary_2012}}      
    \label{fig:hannoi}
\end{figure}

However, Python seems to attract a large community of developers interested in data analysis, web development, system administration, machine learning---according to a survey conducted in 2018 by JetBrains---\footnote{\url{https://www.jetbrains.com/research/python-developers-survey-2018/}}one can fear that the wide adoption of dynamic programming languages, like Python, in production may critically hamper the power consumption of ICT.

As the popularity of such dynamic programming languages partly builds on the wealth and the diversity of their ecosystem (\emph{e.g.}, the NumPY, SciKit\,Learn, and Panda libraries in Python), one cannot reasonably expect that developers will likely move to an alternative programming language mostly for energy considerations.
Rather, we believe that a better option consists in leveraging the strength of this rich ecosystem to promote energy-efficient solutions in order to improve the power consumption of legacy software systems.

In this article, we therefore explore the oblivious optimizations that can be applied to Python legacy applications in order to reduce their energy footprint.
As Python is widely adopted by software services deployed in public and private Cloud infrastructures, we believe that our contributions will benefit to wide diversity of legacy systems and not only favorably contribute to reduce the carbon emissions of ICT, but also reduce their cloud invoice for the resources consumed by these services.
More specifically, this article focuses on runtime optimizations that can adopted by developers to leverage the power consumption of Python applications.
These optimizations include not only alternative interpreters, but also \emph{ahead-of-time} (AOT) compilation and \emph{just-in-time} (JIT) libraries that are maintained by the community.

The contributions of this article can therefore be summarized as:
\begin{compactenum}
    \item a classification of state-of-the-art Python optimizations,
    \item an evaluation of the power efficiency of these optimizations on well-known micro-benchmarks,
    \item a comparison of the effective energy efficiency of Python compared to compiled languages,
    \item an assessment of the energy speedup of these optimizations on representative applications and workloads.
\end{compactenum}


The remainder of this article is organized as follows.
Section\dots


% While this power optimization objective may imply to constrain developers with tools and methods to enforce the adoption of best practices with regards to power efficiency, we believe that another axis can be investigated to deliver seamless improvements without changing the developers' practices on the short-term.
% Given that most of modern programming languages are implemented by \emph{virtual machines} (\emph{e.g.}, Java, Python), we consider that improvements achieved at the level of virtual machines will indirectly benefit to a large spectrum of applications without necessarily impacting the software developers.

% However, Python may also be perceived as delivering poor power efficiency, because of its interpreter.
% In this article, we are therefore interested in analyzing the opportunities for improving the power efficiency of Python software systems without relying on the developer expertise.
% Typically, while benefiting from the comfort of their favorite programming language, we are interested in exposing runtime options to the developers that can help them to substantially improve the power efficiency of their software systems without learning a new language or adjusting their programming style.
